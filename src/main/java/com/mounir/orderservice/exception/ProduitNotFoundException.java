package com.mounir.orderservice.exception;

public class ProduitNotFoundException extends RuntimeException {

    public ProduitNotFoundException(Long id) {
        super("Product not found for id = " + id);
    }
}