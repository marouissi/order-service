package com.mounir.orderservice.entities;

import javax.persistence.*;
import java.math.BigDecimal;

/**

 SQL QUERY:

 create table Produit (
 id serial PRIMARY KEY,
 code varchar(255),
 marque varchar(255),
 modele varchar(255),
 caracteristiques varchar(255),
 prix_unitaire decimal,
 quantite integer,
 category_id integer,
 CONSTRAINT fk_categorie
 FOREIGN KEY(category_id)
 REFERENCES categorie(id)
 );
 *
 */

@Entity
public class Produit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String code;

    private String marque;

    private String modele;

    private String caracteristiques;

    private BigDecimal prixUnitaire;

    private Long quantite;

    @ManyToOne()
    @JoinColumn(name = "category_id")
    private Categorie categorie;

    public Produit() {
    }

    public Produit(String code, String marque, String modele, String caracteristiques, BigDecimal prixUnitaire, Long quantite, Categorie categorie) {
        this.code = code;
        this.marque = marque;
        this.modele = modele;
        this.caracteristiques = caracteristiques;
        this.prixUnitaire = prixUnitaire;
        this.quantite = quantite;
        this.categorie = categorie;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public String getCaracteristiques() {
        return caracteristiques;
    }

    public void setCaracteristiques(String caracteristiques) {
        this.caracteristiques = caracteristiques;
    }

    public BigDecimal getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(BigDecimal prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public Long getQuantite() {
        return quantite;
    }

    public void setQuantite(Long quantite) {
        this.quantite = quantite;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }
}
