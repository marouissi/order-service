package com.mounir.orderservice.repository;

import com.mounir.orderservice.entities.Commande;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommandeRepository extends JpaRepository<Commande, Long> {
}
