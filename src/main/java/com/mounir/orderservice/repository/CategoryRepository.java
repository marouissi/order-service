package com.mounir.orderservice.repository;

import com.mounir.orderservice.entities.Categorie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Categorie, Long> {

    Categorie findCategorieByLibelle(String libelle);
}
