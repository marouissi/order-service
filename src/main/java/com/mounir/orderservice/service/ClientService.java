package com.mounir.orderservice.service;

import com.mounir.orderservice.entities.Client;
import com.mounir.orderservice.payload.ClientPayload;
import com.mounir.orderservice.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientService {

    @Autowired private ClientRepository clientRepository;

    public ClientPayload create(ClientPayload clientPayload) {
        Client client = new Client(clientPayload.getNom(), clientPayload.getPrenom(), clientPayload.getDateNaissance(),
                clientPayload.getAddresse(), clientPayload.getVille(), clientPayload.getTel(), clientPayload.getFax(),
                clientPayload.getGsm(), clientPayload.getEmail());
        client = clientRepository.save(client);
        clientPayload.setId(client.getId());
        return clientPayload;
    }

    public ClientPayload update(ClientPayload clientPayload) {
        //TODO: handle the case where the client is not found.(throw an error )
        clientRepository.findById(clientPayload.getId()).ifPresent(
            client -> {
                client.setNom(clientPayload.getNom());
                client.setPrenom(clientPayload.getPrenom());
                client.setVille(clientPayload.getVille());
                client.setAddresse(clientPayload.getAddresse());
                client.setCodePostale(clientPayload.getCodePostale());
                client.setEmail(clientPayload.getEmail());
                client.setFax(clientPayload.getFax());
                client.setDateNaissance(clientPayload.getDateNaissance());
                client.setTel(clientPayload.getTel());
                client.setGsm(clientPayload.getGsm());
                clientRepository.save(client);
            }
        );
        return clientPayload;
    }

    public List<ClientPayload>  findAll() {
        List<ClientPayload> clients = clientRepository.findAll()
                .stream()
                .map(client ->
                        new ClientPayload(client.getId(), client.getNom(), client.getPrenom(), client.getDateNaissance(),
                                client.getAddresse(), client.getVille(), client.getCodePostale(), client.getTel(), client.getFax(),
                                client.getGsm(), client.getEmail()))
                .collect(Collectors.toList());
        return clients;
    }

    public ClientPayload findById(long id) {
         return clientRepository.findById(id).map(client ->
                 new ClientPayload(client.getId(), client.getNom(), client.getPrenom(), client.getDateNaissance(),
                         client.getAddresse(), client.getVille(), client.getCodePostale(), client.getTel(), client.getFax(),
                         client.getGsm(), client.getEmail())).orElseThrow(() -> new RuntimeException("Client with id " + id + " not found"));
    }
}
