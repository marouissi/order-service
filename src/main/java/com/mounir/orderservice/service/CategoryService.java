package com.mounir.orderservice.service;

import com.mounir.orderservice.entities.Categorie;
import com.mounir.orderservice.payload.CategoryPayload;
import com.mounir.orderservice.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;


    public CategoryPayload create(CategoryPayload categoryPayload) {
        Categorie categorie = new Categorie(categoryPayload.getCode(), categoryPayload.getLibelle());
        categorie = categoryRepository.save(categorie);
        categoryPayload.setId(categorie.getId());
        return categoryPayload;
    }


    public List<CategoryPayload> findAll(){
        List<Categorie> categories = categoryRepository.findAll();
        List<CategoryPayload> payload = categories
                .stream()
                .map(cat -> new CategoryPayload(cat.getId(), cat.getCode(), cat.getLibelle()))
                .collect(Collectors.toList());
        return payload;
    }

    public Optional<CategoryPayload> findById(Long id) {
         Optional<Categorie> categorie = categoryRepository.findById(id);
        Optional<CategoryPayload> found = categorie.map(cat -> new CategoryPayload(cat.getId(), cat.getCode(), cat.getLibelle()));
         return found;
    }

}
