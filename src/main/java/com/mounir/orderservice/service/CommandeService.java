package com.mounir.orderservice.service;

import com.mounir.orderservice.entities.Client;
import com.mounir.orderservice.entities.Commande;
import com.mounir.orderservice.entities.LigneCommande;
import com.mounir.orderservice.entities.Produit;
import com.mounir.orderservice.payload.CommandeReponse;
import com.mounir.orderservice.payload.CommandeRequest;
import com.mounir.orderservice.payload.LigneCommandeReponse;
import com.mounir.orderservice.repository.ClientRepository;
import com.mounir.orderservice.repository.CommandeRepository;
import com.mounir.orderservice.repository.ProduitRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommandeService {

    private final CommandeRepository commandeRepository;
    private final ProduitRepository produitRepository;
    private final ClientRepository clientRepository;


    public CommandeService(CommandeRepository commandeRepository, ProduitRepository produitRepository, ClientRepository clientRepository) {
        this.commandeRepository = commandeRepository;
        this.produitRepository = produitRepository;
        this.clientRepository = clientRepository;
    }

    public CommandeReponse create(CommandeRequest commandePayload) {

        List<LigneCommande> lignesCommande = commandePayload.getCommandes()
                .stream()
                .map(ligne -> {
                    Produit produit = produitRepository.findById(ligne.getProduitId()).orElseThrow(() -> new RuntimeException("produit non trouve "));
                    return new LigneCommande(ligne.getQuantite(), 1l, produit);
                })
                .collect(Collectors.toList());

        Client client = clientRepository.findById(commandePayload.getClientId()).orElseThrow(() -> new RuntimeException("Client not found"));

        Commande commande = new Commande(1l,  client, lignesCommande);
        commande = commandeRepository.save(commande);

       List<LigneCommandeReponse> ligneCommandeReponses = commande.getLigneCommandes().stream().map(ligneCommande -> {
            LigneCommandeReponse ligne = new LigneCommandeReponse(ligneCommande.getQte(), ligneCommande.getPrixUnitaire(),
                    ligneCommande.getPrixTotal(), ligneCommande.getProduit().getCode(), ligneCommande.getProduit().getId());
            return ligne;
        }).collect(Collectors.toList());

        return new CommandeReponse(commande.getNumero(), commande.getDate(), commande.getPrixTotal(), commande.getEtat(), ligneCommandeReponses);
    }


    //TODO: findCommandeByClient.

}
