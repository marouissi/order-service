package com.mounir.orderservice.service;

import com.mounir.orderservice.exception.CategoryNotFoundException;
import com.mounir.orderservice.exception.ProduitNotFoundException;
import com.mounir.orderservice.repository.ProduitRepository;
import com.mounir.orderservice.entities.Categorie;
import com.mounir.orderservice.entities.Produit;
import com.mounir.orderservice.payload.CategoryPayload;
import com.mounir.orderservice.payload.ProduitPayload;
import com.mounir.orderservice.repository.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProduitService {

    private ProduitRepository produitRepository;
    private CategoryRepository categoryRepository;

    /**
    * Constructor Injection. Not need to autowire the attributes.
     */
    public ProduitService(ProduitRepository produitRepository, CategoryRepository categoryRepository) {
        this.produitRepository = produitRepository;
        this.categoryRepository = categoryRepository;
    }

    public ProduitPayload create(ProduitPayload produitPayload) {
        Categorie categorie = categoryRepository.findById(produitPayload.getCategorie().getId())
                .orElseThrow(() -> new CategoryNotFoundException(produitPayload.getCategorie().getId()));
        Produit produit = new Produit(produitPayload.getCode(), produitPayload.getMarque(), produitPayload.getModele(),
                produitPayload.getCaracteristiques(), produitPayload.getPrixUnitaire(), produitPayload.getQuantite(), categorie);
        produit = produitRepository.save(produit);
        produitPayload.setId(produit.getId());
        return produitPayload;
    }

    public ProduitPayload update(ProduitPayload produitPayload) {
        Categorie categorie = categoryRepository.findById(produitPayload.getCategorie().getId())
                .orElseThrow(() -> new RuntimeException("Category not found."));

        Produit produit = produitRepository.findById(produitPayload.getId()).orElseThrow(() -> new RuntimeException("produit not found"));
        produit.setCaracteristiques(produitPayload.getCaracteristiques());
        produit.setCategorie(categorie);
        produit.setMarque(produitPayload.getMarque());
        produit.setModele(produitPayload.getModele());
        produit.setPrixUnitaire(produitPayload.getPrixUnitaire());
        produit.setQuantite(produitPayload.getQuantite());
        produit.setCode(produitPayload.getCode());
        produitRepository.save(produit);
        return produitPayload;
    }

    public List<ProduitPayload> findAll(){
        List<Produit> produits = produitRepository.findAll();
        List<ProduitPayload> produitsPayloads = produits
                .stream()
                .map(produit -> new ProduitPayload(produit.getId(), produit.getCode(), produit.getMarque(), produit.getModele(),
                        produit.getCaracteristiques(), produit.getPrixUnitaire(), produit.getQuantite(), getCategorie(produit)))
                .collect(Collectors.toList());

        return produitsPayloads;
    }

    public ProduitPayload findById(Long id) {
        ProduitPayload produitPayload = produitRepository.findById(id)
                .map(produit -> new ProduitPayload(produit.getId(), produit.getCode(), produit.getMarque(),
                        produit.getModele(), produit.getCaracteristiques(), produit.getPrixUnitaire(), produit.getQuantite(),
                        getCategorie(produit))).orElseThrow(() -> new ProduitNotFoundException(id));
        return produitPayload;
    }
    
    private CategoryPayload getCategorie(Produit produit) {
        return new CategoryPayload(produit.getCategorie().getId(), produit.getCategorie().getCode(), produit.getCategorie().getLibelle());
    }

}
