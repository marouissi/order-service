package com.mounir.orderservice.payload;

import java.math.BigDecimal;

public class ProduitPayload {

    private Long id;

    private String code;

    private String marque;

    private String modele;

    private String caracteristiques;

    private BigDecimal prixUnitaire;

    private Long quantite;

    private CategoryPayload categorie;

    public ProduitPayload() {

    }

    public ProduitPayload(Long id, String code, String marque, String modele, String caracteristiques, BigDecimal prixUnitaire, Long quantite, CategoryPayload categorie) {
        this.id = id;
        this.code = code;
        this.marque = marque;
        this.modele = modele;
        this.caracteristiques = caracteristiques;
        this.prixUnitaire = prixUnitaire;
        this.quantite = quantite;
        this.categorie = categorie;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public String getCaracteristiques() {
        return caracteristiques;
    }

    public void setCaracteristiques(String caracteristiques) {
        this.caracteristiques = caracteristiques;
    }

    public BigDecimal getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(BigDecimal prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public Long getQuantite() {
        return quantite;
    }

    public void setQuantite(Long quantite) {
        this.quantite = quantite;
    }

    public CategoryPayload getCategorie() {
        return categorie;
    }

    public void setCategorie(CategoryPayload categorie) {
        this.categorie = categorie;
    }
}
