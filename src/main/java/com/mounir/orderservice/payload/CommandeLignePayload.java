package com.mounir.orderservice.payload;

public class CommandeLignePayload {

    private long produitId;

    private long quantite;

    public CommandeLignePayload(){
    }

    public CommandeLignePayload(long produitId, long quantite) {
        this.produitId = produitId;
        this.quantite = quantite;
    }

    public long getProduitId() {
        return produitId;
    }

    public void setProduitId(long produitId) {
        this.produitId = produitId;
    }

    public long getQuantite() {
        return quantite;
    }

    public void setQuantite(long quantite) {
        this.quantite = quantite;
    }
}
