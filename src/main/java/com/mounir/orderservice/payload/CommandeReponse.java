package com.mounir.orderservice.payload;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public class CommandeReponse {

    private String numeroCommande;
    private LocalDate date;
    private BigDecimal prixTotal;
    private Long etat;
    private List<LigneCommandeReponse> ligneCommandes;

    public CommandeReponse() {

    }

    public CommandeReponse(String numeroCommande, LocalDate date, BigDecimal prixTotal, Long etat, List<LigneCommandeReponse> ligneCommandes) {
        this.numeroCommande = numeroCommande;
        this.date = date;
        this.prixTotal = prixTotal;
        this.etat = etat;
        this.ligneCommandes = ligneCommandes;
    }

    public String getNumeroCommande() {
        return numeroCommande;
    }

    public void setNumeroCommande(String numeroCommande) {
        this.numeroCommande = numeroCommande;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public BigDecimal getPrixTotal() {
        return prixTotal;
    }

    public void setPrixTotal(BigDecimal prixTotal) {
        this.prixTotal = prixTotal;
    }

    public Long getEtat() {
        return etat;
    }

    public void setEtat(Long etat) {
        this.etat = etat;
    }

    public List<LigneCommandeReponse> getLigneCommandes() {
        return ligneCommandes;
    }

    public void setLigneCommandes(List<LigneCommandeReponse> ligneCommandes) {
        this.ligneCommandes = ligneCommandes;
    }
}
