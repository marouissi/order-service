package com.mounir.orderservice.payload;

import java.math.BigDecimal;

public class LigneCommandeReponse {

    private Long quantite;
    private BigDecimal prixUnitaire;
    private BigDecimal prixTotal;
    private String codeProduit;
    private Long produitID;

    public LigneCommandeReponse(){

    }

    public LigneCommandeReponse(Long quantite, BigDecimal prixUnitaire, BigDecimal prixTotal, String codeProduit, Long produitID) {
        this.quantite = quantite;
        this.prixUnitaire = prixUnitaire;
        this.prixTotal = prixTotal;
        this.codeProduit = codeProduit;
        this.produitID = produitID;
    }

    public Long getQuantite() {
        return quantite;
    }

    public void setQuantite(Long quantite) {
        this.quantite = quantite;
    }

    public BigDecimal getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(BigDecimal prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public BigDecimal getPrixTotal() {
        return prixTotal;
    }

    public void setPrixTotal(BigDecimal prixTotal) {
        this.prixTotal = prixTotal;
    }

    public String getCodeProduit() {
        return codeProduit;
    }

    public void setCodeProduit(String codeProduit) {
        this.codeProduit = codeProduit;
    }

    public Long getProduitID() {
        return produitID;
    }

    public void setProduitID(Long produitID) {
        this.produitID = produitID;
    }
}
