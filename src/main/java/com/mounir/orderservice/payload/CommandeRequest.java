package com.mounir.orderservice.payload;

import java.time.LocalDate;
import java.util.List;

public class CommandeRequest {

    private String numero;
    private LocalDate date;
    private Long clientId;
    private List<CommandeLignePayload> commandes;

    public CommandeRequest(){
    }

    public CommandeRequest(String numero, LocalDate date, List<CommandeLignePayload> commandes) {
        this.numero = numero;
        this.date = date;
        this.commandes = commandes;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public List<CommandeLignePayload> getCommandes() {
        return commandes;
    }

    public void setCommandes(List<CommandeLignePayload> commandes) {
        this.commandes = commandes;
    }
}
