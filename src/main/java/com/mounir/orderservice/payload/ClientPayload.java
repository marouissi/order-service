package com.mounir.orderservice.payload;

import java.time.LocalDate;

public class ClientPayload {

    private long id;

    private String nom;

    private String prenom;

    private LocalDate dateNaissance;

    private String addresse;

    private String ville;

    private Long codePostale;

    private String tel;

    private String fax;

    private String gsm;

    private String email;

    /**
     * this is required for Jackson to instantiate the the class before start mapping values.
     */
    public ClientPayload() {

    }

    public ClientPayload(String nom, String prenom, LocalDate dateNaissance, String addresse, String ville, Long codePostale, String tel, String fax, String gsm, String email) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.addresse = addresse;
        this.ville = ville;
        this.codePostale = codePostale;
        this.tel = tel;
        this.fax = fax;
        this.gsm = gsm;
        this.email = email;
    }

    public ClientPayload(long id, String nom, String prenom, LocalDate dateNaissance, String addresse, String ville, Long codePostale, String tel, String fax, String gsm, String email) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.addresse = addresse;
        this.ville = ville;
        this.codePostale = codePostale;
        this.tel = tel;
        this.fax = fax;
        this.gsm = gsm;
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getAddresse() {
        return addresse;
    }

    public void setAddresse(String addresse) {
        this.addresse = addresse;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public Long getCodePostale() {
        return codePostale;
    }

    public void setCodePostale(Long codePostale) {
        this.codePostale = codePostale;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getGsm() {
        return gsm;
    }

    public void setGsm(String gsm) {
        this.gsm = gsm;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
