package com.mounir.orderservice.payload;

public class CategoryPayload {

    private Long id;
    private String code;
    private String libelle;

    public CategoryPayload(){

    }

    public CategoryPayload(String code, String libelle) {
        this.code = code;
        this.libelle = libelle;
    }

    public CategoryPayload(Long id, String code, String libelle) {
        this.id = id;
        this.code = code;
        this.libelle = libelle;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
