package com.mounir.orderservice.endpoint;

import com.mounir.orderservice.payload.ClientPayload;
import com.mounir.orderservice.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("client")
public class ClientEndPoint {

    @Autowired
    private ClientService clientService;

    @PostMapping("")
    public ResponseEntity<ClientPayload> create(@RequestBody ClientPayload clientPayload) {
        ClientPayload client = clientService.create(clientPayload);
        return ResponseEntity.status(HttpStatus.CREATED).body(client);
    }

    @GetMapping("/all")
    public ResponseEntity<List<ClientPayload>> findAll() {
        List<ClientPayload> clients = clientService.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(clients);
    }

    @PutMapping()
    public ResponseEntity<ClientPayload> update(@RequestBody ClientPayload clientPayload) {
        ClientPayload updatedClient = clientService.update(clientPayload);
        return ResponseEntity.status(HttpStatus.OK).body(updatedClient);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ClientPayload> findById(@PathVariable Long id) {
        ClientPayload clientPayload = clientService.findById(id);
        return ResponseEntity.status(HttpStatus.OK).body(clientPayload);
    }
}
