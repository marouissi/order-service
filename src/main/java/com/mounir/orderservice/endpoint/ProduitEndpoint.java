package com.mounir.orderservice.endpoint;

import com.mounir.orderservice.payload.ProduitPayload;
import com.mounir.orderservice.service.ProduitService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/produit")
public class ProduitEndpoint {

    public ProduitService produitService;

    public ProduitEndpoint(ProduitService produitService) {
        this.produitService = produitService;
    }

    @PostMapping()
    public ResponseEntity<ProduitPayload> create(@RequestBody ProduitPayload produitPayload){
          produitPayload = produitService.create(produitPayload);
          return ResponseEntity.status(HttpStatus.CREATED).body(produitPayload);
    }

    @PutMapping
    public ResponseEntity<ProduitPayload> update(@RequestBody ProduitPayload produitPayload){
        produitPayload = produitService.update(produitPayload);
        return ResponseEntity.status(HttpStatus.OK).body(produitPayload);
    }

    @GetMapping
    public ResponseEntity<List<ProduitPayload>> findAll() {
        List<ProduitPayload> produits = produitService.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(produits);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProduitPayload> findById(@PathVariable Long id) {
        ProduitPayload produit = produitService.findById(id);
        return ResponseEntity.ok().body(produit);
    }
}
