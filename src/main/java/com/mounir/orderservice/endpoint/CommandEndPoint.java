package com.mounir.orderservice.endpoint;

import com.mounir.orderservice.payload.CommandeReponse;
import com.mounir.orderservice.payload.CommandeRequest;
import com.mounir.orderservice.service.CommandeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/commande")
public class CommandEndPoint {

    //TODO: add test unitaire.

    private final CommandeService commandeService;

    public CommandEndPoint(CommandeService commandeService) {
        this.commandeService = commandeService;
    }

    @PostMapping
    public ResponseEntity<CommandeReponse> createCommande(@RequestBody CommandeRequest request) {
        CommandeReponse commande = commandeService.create(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(commande);
    }
}
