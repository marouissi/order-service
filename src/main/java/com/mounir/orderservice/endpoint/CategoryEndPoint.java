package com.mounir.orderservice.endpoint;

import com.mounir.orderservice.payload.CategoryPayload;
import com.mounir.orderservice.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class CategoryEndPoint {

    
    private final CategoryService categoryService;
    
    public CategoryEndPoint(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
    
    
    @PostMapping("/category")
    public ResponseEntity<CategoryPayload> create(@RequestBody CategoryPayload categoryPayload){
        return ResponseEntity.status(HttpStatus.CREATED).body(categoryService.create(categoryPayload));
    }

    @GetMapping("/category/all")
    public ResponseEntity<List<CategoryPayload>> findAll() {
        List<CategoryPayload> categories = categoryService.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(categories);
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<Optional<CategoryPayload>> findById(@PathVariable Long id) {
        Optional<CategoryPayload> categorie = categoryService.findById(id);
        return ResponseEntity.status(HttpStatus.OK).body(categorie);
    }
}
