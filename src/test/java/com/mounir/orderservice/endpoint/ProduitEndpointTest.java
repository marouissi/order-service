package com.mounir.orderservice.endpoint;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.mounir.orderservice.payload.CategoryPayload;
import com.mounir.orderservice.payload.ProduitPayload;
import com.mounir.orderservice.service.ProduitService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.containsInAnyOrder;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


/**
 * Documentation: https://www.baeldung.com/spring-boot-testing
 */

@WebMvcTest(ProduitEndpoint.class)
@ExtendWith(SpringExtension.class)
class ProduitEndpointTest {

    @Autowired private MockMvc mockMvc;

    @MockBean private ProduitService produitService;

    private ObjectMapper mapper;

    private CategoryPayload category;

    @BeforeEach
    void setUp(){
        mapper = new ObjectMapper();
        category = new CategoryPayload("catgeory", "libelle");
    }

    @Test
    void create_produit_shouldReturn_Created_status() throws Exception{
        ProduitPayload productPayload = new ProduitPayload(null,"code1", "marque1", "model-produit", "caracteristique-produit", new BigDecimal(15.99), 20L, category);

        /**
         * This will spy on the mock produitService.create extract the argument 0  produitPayload and set the id to 11.
         */
        Long createdId = 11L;
        when(produitService.create(any(ProduitPayload.class))).thenAnswer(invocationOnMock -> {
            ProduitPayload produitPayloadSaved = invocationOnMock.getArgument(0);
            produitPayloadSaved.setId(createdId);
            return produitPayloadSaved;
        });

        mockMvc.perform(post("/produit")
                        .content(mapper.writeValueAsString(productPayload))
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(11)));
    }

    @Test
    void update_produit_shouldReturn_http_status_OK() throws Exception{
        ProduitPayload productPayload = new ProduitPayload(12L,"updatedCode", "UpdatedCode", "updatedModel",
                "updated-caracteristique", new BigDecimal(19.99), 20L, category);

        when(produitService.update(any(ProduitPayload.class))).thenReturn(productPayload);

        mockMvc.perform(put("/produit")
                        .content(mapper.writeValueAsString(productPayload))
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(12)));
    }

    @Test
    void findAll_returns_Ok_http_status_with_list_of_product() throws Exception{
        CategoryPayload category = new CategoryPayload("catgeory", "libelle");
        ProduitPayload firstProduct = new ProduitPayload(1L,"code1", "marque1", "model-produit1", "caracteristique-produit", new BigDecimal(15.99), 20L, category);
        ProduitPayload secondProduct = new ProduitPayload(2L,"code2", "marque2", "model-produit2", "caracteristique-produit", new BigDecimal(15.99), 40L, category);

        when(produitService.findAll()).thenReturn(Arrays.asList(firstProduct, secondProduct));

        mockMvc.perform(get("/produit")).
                andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.*",hasSize(2)))
                .andExpect(jsonPath("$[*].id", containsInAnyOrder(1, 2)))
                .andExpect(jsonPath("$[*].code", containsInAnyOrder("code1", "code2")))
                .andExpect(jsonPath("$[*].marque", containsInAnyOrder("marque1", "marque2")))
                .andExpect(jsonPath("$[*].modele", containsInAnyOrder("model-produit1", "model-produit2")))
                .andExpect(jsonPath("$[*].quantite", containsInAnyOrder(20, 40)));
    }

    @Test
    void findAll_shouldReturn_Ok_http_status_with_empty_list_of_product() throws Exception{
        when(produitService.findAll()).thenReturn(Collections.emptyList());

        mockMvc.perform(get("/produit")).
                andExpect(status().isOk())
                .andExpect(jsonPath("$.*",hasSize(0)));
    }

    @Test
    void findBy_shouldReturn_Ok_with_Produit() throws Exception {
        ProduitPayload product = new ProduitPayload(1L,"code", "marque", "model-produit",
                "caracteristique-produit", new BigDecimal(15.99), 20L, category);
        when(produitService.findById(eq(1L))).thenReturn(product);

        mockMvc.perform(get("/produit/{id}", 1)).
                andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.code", is("code")))
                .andExpect(jsonPath("$.marque", is("marque")))
                .andExpect(jsonPath("$.modele", is("model-produit")))
                .andExpect(jsonPath("$.quantite", is(20)));
    }
}