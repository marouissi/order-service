package com.mounir.orderservice.service;

import com.mounir.orderservice.entities.Categorie;
import com.mounir.orderservice.entities.Produit;
import com.mounir.orderservice.exception.CategoryNotFoundException;
import com.mounir.orderservice.exception.ProduitNotFoundException;
import com.mounir.orderservice.payload.CategoryPayload;
import com.mounir.orderservice.payload.ProduitPayload;
import com.mounir.orderservice.repository.CategoryRepository;
import com.mounir.orderservice.repository.ProduitRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.junit.jupiter.MockitoExtension;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.Captor;
import org.mockito.ArgumentCaptor;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.never;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@ExtendWith(MockitoExtension.class)
class ProduitServiceTest {

    @Mock
    private ProduitRepository produitRepository;

    @Mock
    private CategoryRepository categoryRepository;

    @InjectMocks
    private ProduitService produitService;

    private CategoryPayload categoryPayload;

    private Categorie categorie;

    private Long categoryId = 123L;

    @Captor
    private ArgumentCaptor<Produit> productCaptor;

    @BeforeEach
    void setUp() {
        categoryPayload = new CategoryPayload(categoryId , "catgeory", "libelle");
        categorie = new Categorie("catgeory", "libelle");
        categorie.setId(categoryId);
    }

    @Test
    void createProduit_Test_Happy_Scenario() {
        long generatedProductId = 7777L;
        ProduitPayload product = new ProduitPayload(null,"code", "marque", "model-produit",
                "caracteristique-produit", new BigDecimal(15.99), 20L, categoryPayload);

        when(categoryRepository.findById(categoryId)).thenReturn(Optional.of(categorie));
        when(produitRepository.save(any())).thenAnswer(invocationOnMock -> {
            Produit savedProduct = invocationOnMock.getArgument(0);
            savedProduct.setId(generatedProductId);
            return savedProduct;
        });

        ProduitPayload result  = produitService.create(product);

        assertThat(result.getId()).isEqualTo(generatedProductId);
        verify(produitRepository, times(1)).save(productCaptor.capture());  // Verify that save method has been called and capture the argument Produit to assert the saved data

        Produit savedProduit = productCaptor.getValue();
        assertThat(savedProduit.getCode()).isEqualTo(product.getCode());
        assertThat(savedProduit.getMarque()).isEqualTo(product.getMarque());
        assertThat(savedProduit.getCaracteristiques()).isEqualTo(product.getCaracteristiques());
        assertThat(savedProduit.getModele()).isEqualTo(product.getModele());
        assertThat(savedProduit.getPrixUnitaire()).isEqualTo(product.getPrixUnitaire());
        assertThat(savedProduit.getQuantite()).isEqualTo(product.getQuantite());
    }

    @Test
    void createProduit_ThrowsError_When_Category_NotFound() {

        ProduitPayload product = new ProduitPayload(null,"code", "marque", "model-produit",
                "caracteristique-produit", new BigDecimal(15.99), 20L, categoryPayload);

        when(categoryRepository.findById(categoryId)).thenReturn(Optional.empty());

        CategoryNotFoundException exception = Assertions.assertThrows(CategoryNotFoundException.class, () -> produitService.create(product));
        assertThat(exception.getMessage()).isEqualTo("Category not found for id = " + categoryId);
        verify(produitRepository, never()).save(any());
    }

    @Test
    void findAll_Test_Happy_scenario() {
        Produit p1 = new Produit("c1", "m1", "mo1","ct1", new BigDecimal(23.99), 100L, categorie);
        p1.setId(23L);
        Produit p2 = new Produit("c2", "m2", "mo2","ct2", new BigDecimal(55.99), 300L, categorie);
        p1.setId(24L);

        when(produitRepository.findAll()).thenReturn(Arrays.asList(p1, p2));

        List<ProduitPayload> produits = produitService.findAll();

        assertThat(produits).hasSize(2);
        verify(produitRepository, times(1)).findAll(); // verify that findAll method has been called only once.
    }

    @Test
    void findAll_Test_Empty_List() {
        //TODO:
    }

    @Test
    void findById_Test_Happy_scenario() {
        long productId = 1234L;
        Produit product = new Produit("c1", "m1", "mo1","ct1", new BigDecimal(23.99), 100L, categorie);
        product.setId(productId);
        when(produitRepository.findById(productId)).thenReturn(Optional.of(product));

        ProduitPayload productResult = produitService.findById(productId);
        assertThat(productResult.getId()).isEqualTo(productId);
        assertThat(productResult.getCode()).isEqualTo(product.getCode());
        assertThat(productResult.getModele()).isEqualTo(product.getModele());
        assertThat(productResult.getMarque()).isEqualTo(product.getMarque());
        assertThat(productResult.getCaracteristiques()).isEqualTo(product.getCaracteristiques());
        assertThat(productResult.getPrixUnitaire()).isEqualTo(product.getPrixUnitaire());
        assertThat(productResult.getQuantite()).isEqualTo(product.getQuantite());
        verify(produitRepository, times(1)).findById(productId);
    }

    @Test
    void findById_Test_produit_NotFound() {
        long productId = 1234L;
        when(produitRepository.findById(productId)).thenReturn(Optional.empty());

        ProduitNotFoundException exception = Assertions.assertThrows(ProduitNotFoundException.class, () -> produitService.findById(productId));
        assertThat(exception.getMessage()).isEqualTo("Product not found for id = " + productId);
        verify(produitRepository, times(1)).findById(productId);
    }

    //TODO: finish the rest of method.
}